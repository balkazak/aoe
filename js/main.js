$(document).ready(function () {
    $('.ham_btn').on('click', function () {
        $('.ham_line1').toggleClass('active_hl1');
        $('.ham_line2').toggleClass('active_hl2');
        $('.ham_line3').toggleClass('active_hl3');
        $('.mobile_menu').toggleClass('active');
    });

    $('a').on('click', function () {
        if ($('.mobile_menu').hasClass("active")) {
            $('.mobile_menu').removeClass('active');
            $('.ham_line1').removeClass('active_hl1');
            $('.ham_line2').removeClass('active_hl2');
            $('.ham_line3').removeClass('active_hl3');
        }
    });


    $('#header').mousemove(function (event) {
        let w = $(window).width();
        let h = $(window).height();
        let bgX = 0;
        let bgY = 0;

        bgX = 50 + 10 * ((event.pageX - w / 2) / (w / 2));
        bgY = 50 + 10 * ((event.pageY - h / 2) / (h / 2));
        $('#header').css('background-position', bgX + '% ' + bgY + '%');
        $('#cloud1').css('left', (bgX - 100) + 'px');
        $('#cloud2').css('right', (bgX * 3 - 300) + 'px');
        $('#cloud3').css('left', (bgX * 3 - 300) + 'px');
        $('#cloud4').css('right', (bgX * 3 - 300) + 'px');
        $('#cloud2').css('bottom', (bgY - 20) + 'px');
        $('#cloud3').css('bottom', (bgY - 30) + 'px');
        $('#cloud4').css('bottom', (bgY * 3 + 20) + 'px');

    });
    $('#footer').mousemove(function (event) {
        let w = $(window).width();
        let bgX = 0;

        bgX = 50 + 10 * ((event.pageX - w / 2) / (w / 2));
        let degrees = 5 * ((event.pageX - w / 2) / (w / 2));
        $('#f_cloud').css('left', (bgX - 100) + 'px');
        if (!$('#f_rocker').hasClass('go')) {
            $('#f_rocker').css('transform', 'rotate(' + degrees + 'deg)');
        }
    });

    $('#f_rocker').on('click', function () {
        $(this).addClass('go');
    });

    $('#left').on('click', function () {
        if ($('#togle').hasClass('right')) {
            $('#togle').removeClass('right');
        }
    });
    $('#right').on('click', function () {
        if (!$('#togle').hasClass('right')) {
            $('#togle').addClass('right');
        }
    });

    $('.dropdown-item').on('click', function () {
        $('.dropdown-toggle').text($(this).text());
    });
});



